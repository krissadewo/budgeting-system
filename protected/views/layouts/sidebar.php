
<?php $this->beginContent('//layouts/main'); ?>

<?php //$this->beginCache(CategoryController::CACHE_SIDEBAR); ?>
<div id="sidebar">

    <ul id="mainNav">
        <li id="navDashboard" class="<?php echo MockController::cssActiveMenu(array('dashboard')); ?>">
            <span class="icon-home"></span>
            <a href="<?php echo $this->createUrl('dashboard/index'); ?>">Dashboard</a>
        </li>

        <li id="navPages" class="<?php echo MockController::cssActiveMenu(array('category', 'item', 'purchase')); ?>">
            <span class="icon-document-alt-stroke"></span>
            <a href="javascript:;">Master Data</a>

            <ul class="subNav">
                <li><a href="<?php echo $this->createUrl('category/index'); ?>">Category</a></li>
                <li><a href="<?php echo $this->createUrl('item/index'); ?>">Item</a></li>
                <li><a href="<?php echo $this->createUrl('purchase/index'); ?>">Purchase</a></li>
                <li><a href="<?php echo $this->createUrl('balance/index'); ?>">Balance</a></li>
                <li><a href="calendar.html">Calendar</a></li>
                <li><a href="stream.html">Stream</a></li>
                <li><a href="gallery.html">Gallery</a></li>
                <li><a href="reports.html">Reports</a></li>
            </ul>
        </li>

        <li id="navForms" class="nav">
            <span class="icon-article"></span>
            <a href="javascript:;">Form Elements</a>

            <ul class="subNav">
                <li><a href="forms.html">Layouts & Elements</a></li>
                <li><a href="forms-validations.html">Validations</a></li>
            </ul>
        </li>

        <li id="navType" class="nav">
            <span class="icon-info"></span>
            <a href="typography.html">Typography</a>
        </li>

        <li id="navGrid" class="nav">
            <span class="icon-layers"></span>
            <a href="grids.html">Grid Layout</a>
        </li>

        <li id="navTables" class="nav">
            <span class="icon-list"></span>
            <a href="tables.html">Tables</a>
        </li>

        <li id="navButtons" class="nav">
            <span class="icon-compass"></span>
            <a href="buttons.html">Buttons & Icons</a>
        </li>

        <li id="navInterface" class="nav">
            <span class="icon-equalizer"></span>
            <a href="interface.html">Interface Elements</a>
        </li>

        <li id="navCharts" class="nav">
            <span class="icon-chart"></span>
            <a href="charts.html">Charts & Graphs</a>
        </li>

        <li id="navMaps" class="nav">
            <span class="icon-map-pin-fill"></span>
            <a href="maps.html">Map Elements</a>
        </li>

        <li class="nav">
            <span class="icon-denied"></span>
            <a href="javascript:;">Error Pages</a>

            <ul class="subNav">
                <li><a href="error-401.html">401 Page</a></li>
                <li><a href="error-403.html">403 Page</a></li>
                <li><a href="error-404.html">404 Page</a></li>
                <li><a href="error-500.html">500 Page</a></li>
                <li><a href="error-503.html">503 Page</a></li>
            </ul>
        </li>
    </ul>

</div> <!-- #sidebar -->


<?php //$this->endCache(); ?>
<div id="content">
    <?php echo $content; ?>
</div> <!-- #content -->
<?php //$this->beginCache(CategoryController::CACHE_SIDEBAR); ?>
<div id="footer">
    Copyright &copy; 2012, MadeByAmp Themes.
</div>

<div id="topNav">
    <ul>
        <li>
            <a href="#menuProfile" class="menu">John Doe</a>

            <div id="menuProfile" class="menu-container menu-dropdown">
                <div class="menu-content">
                    <ul class="">
                        <li><a href="javascript:;">Edit Profile</a></li>
                        <li><a href="javascript:;">Edit Settings</a></li>
                        <li><a href="javascript:;">Suspend Account</a></li>
                    </ul>
                </div>
            </div>
        </li>
        <li><a href="javascript:;">Upgrade</a></li>
        <li><a href="index-2.html">Logout</a></li>
    </ul>
</div> <!-- #topNav -->

<div id="quickNav">
    <ul>
        <li class="quickNavMail">
            <a href="#menuAmpersand" class="menu"><span class="icon-book"></span></a>		



            <div id="menuAmpersand" class="menu-container quickNavConfirm">
                <div class="menu-content cf">							

                    <div class="qnc qnc_confirm">

                        <h3>Confirm</h3>

                        <div class="qnc_item">
                            <div class="qnc_content">
                                <span class="qnc_title">Confirm #1</span>
                                <span class="qnc_preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</span>
                                <span class="qnc_time">3 hours ago</span>
                            </div> <!-- .qnc_content -->

                            <div class="qnc_actions">						
                                <button class="btn btn-primary btn-small">Accept</button>
                                <button class="btn btn-quaternary btn-small">Not Now</button>
                            </div>
                        </div>

                        <div class="qnc_item">
                            <div class="qnc_content">
                                <span class="qnc_title">Confirm #2</span>
                                <span class="qnc_preview">Duis aute irure dolor in henderit in voluptate velit esse cillum dolore.</span>
                                <span class="qnc_time">3 hours ago</span>
                            </div> <!-- .qnc_content -->

                            <div class="qnc_actions">						
                                <button class="btn btn-primary btn-small">Accept</button>
                                <button class="btn btn-quaternary btn-small">Not Now</button>
                            </div>
                        </div>

                        <div class="qnc_item">
                            <div class="qnc_content">
                                <span class="qnc_title">Confirm #3</span>
                                <span class="qnc_preview">Duis aute irure dolor in henderit in voluptate velit esse cillum dolore.</span>
                                <span class="qnc_time">3 hours ago</span>
                            </div> <!-- .qnc_content -->

                            <div class="qnc_actions">						
                                <button class="btn btn-primary btn-small">Accept</button>
                                <button class="btn btn-quaternary btn-small">Not Now</button>
                            </div>
                        </div>

                        <a href="javascript:;" class="qnc_more">View all Confirmations</a>

                    </div> <!-- .qnc -->	
                </div>
            </div>
        </li>
        <li class="quickNavNotification">
            <a href="#menuPie" class="menu"><span class="icon-chat"></span></a>

            <div id="menuPie" class="menu-container">
                <div class="menu-content cf">					

                    <div class="qnc">

                        <h3>Notifications</h3>

                        <a href="javascript:;" class="qnc_item">
                            <div class="qnc_content">
                                <span class="qnc_title">Notification #1</span>
                                <span class="qnc_preview">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do.</span>
                                <span class="qnc_time">3 hours ago</span>
                            </div> <!-- .qnc_content -->
                        </a>

                        <a href="javascript:;" class="qnc_item">
                            <div class="qnc_content">
                                <span class="qnc_title">Notification #2</span>
                                <span class="qnc_preview">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu.</span>
                                <span class="qnc_time">3 hours ago</span>
                            </div> <!-- .qnc_content -->
                        </a>

                        <a href="javascript:;" class="qnc_more">View all Confirmations</a>

                    </div> <!-- .qnc -->
                </div>
            </div>				
        </li>
    </ul>		
</div> <!-- .quickNav -->

<?php //$this->endCache(); ?>
<?php $this->endContent(); ?>
