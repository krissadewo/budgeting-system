<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="language" content="en" />       
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <?php
        ?>
    </head>

    <body>
        <div id="wrapper">

            <div id="header">
                <h1><a href="<?php echo $this->createUrl('dashboard/index'); ?>">Canvas Admin</a></h1>		

                <a href="javascript:;" id="reveal-nav">
                    <span class="reveal-bar"></span>
                    <span class="reveal-bar"></span>
                    <span class="reveal-bar"></span>
                </a>
            </div> <!-- #header -->

            <div id="search">
                <form>
                    <input type="text" name="search" placeholder="Search..." id="searchField" />
                </form>		
            </div> <!-- #search -->

            <?php echo $content; ?>

        </div>

    </body>
</html>

<script>
    $(function() {
        $( "#dialog:ui-dialog" ).dialog( "destroy" );
	
        $( "#dialog-data-not-found" ).dialog({
            autoOpen : false,
            resizable: false,
            modal: true,
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    });
    
    $(function() {
        $( "#dialog:ui-dialog" ).dialog( "destroy" );
	
        $( "#dialog-delete-not-found" ).dialog({
            autoOpen : false,
            resizable: false,
            modal: true,
            buttons: {
                Ok: function() {
                    $( this ).dialog( "close" );
                }
            }
        });
    });
</script>

<!-- This dialog form for used general function -->
<div id="dialog-data-not-found" title="Information">
    <p>
        <br>
            <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
            The data that you want can't be found !
    </p>      
</div>

<div id="dialog-delete-confirm" title="Confirmation">        
    <p>
        <br>
            <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 50px 0;"></span>
            These items will be permanently deleted and cannot be recovered. Are you sure?
    </p>
</div>

<div id="dialog-delete-not-found" title="Information">
    <p>
        <br>
            <span class="ui-icon ui-icon-circle-check" style="float:left; margin:0 7px 50px 0;"></span>
            Please checked your data that you want delete !
    </p>      
</div>
