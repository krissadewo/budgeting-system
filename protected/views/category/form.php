<div id="contentHeader">
    <h1>Item</h1>
</div> <!-- #contentHeader -->
<script type="text/javascript">     
   
    $(document).ready(function(){         
                 
        $(function() {
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
	
            $( "#dialog-delete-confirm" ).dialog({
                autoOpen : false,
                resizable: false,
                height:190,
                width: 350,
                modal: true,
                buttons: {
                    "Delete ": function() {
                        $.ajax({
                            url         : '<?php echo $this->createUrl("category/delete"); ?>',
                            data        : {id : $("#category-id").val()},
                            dataType    : 'json',
                            type        : 'get'                            
                        }).done(function(msg){
                            if(msg['success']){
                                alert(msg['success']);  
                                $(location).attr('href', '<?php echo $this->createUrl('category/index'); ?>');      
                            }else{
                                alert("Failed");
                            }
                        });                           
                       
                        $( this ).dialog( "close" );
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }                    
                }
            });
        });
       
        $( ".table.table-striped" ).on('click','.choose',function() {
            $("#search-result" ).dialog( "close" );            
        });
        
        $("#back-item").button().click(function(){          
            $(location).attr('href', '<?php echo $this->createUrl('category/index'); ?>'); 
        });
        
        $("#delete-item").button().click(function(){
            $("#dialog-delete-confirm").dialog("open");
        });
        
        $("#save-item").button().click(function(){
            $(location).attr('href', '<?php echo $this->createUrl('category/index'); ?>'); 
        });
        
        $( "#search-category" ).button().click(function() {           
            searchCategory();
            $( "#search-result" ).dialog( "open" );
        });
        
        $("#key-name").change(function(){
            searchCategory();
        });
            
    });         
       
</script>
<div class="container">    
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'category-form',
        'enableAjaxValidation' => FALSE,
            ));
    ?>
    <?php echo $form->hiddenField($category, 'id', array('id' => 'category-id')); ?>
    <div class = "grid-24">
        <div class = "box plain">
            <div class = "dataTables_filter">
                <button type="submit" class = "btn btn-quaternary" id = "save-item">
                    <span class = "icon-document-alt-stroke"></span>
                    Save
                </button>
                <button type="button" class = "btn btn-quaternary" id = "delete-item">
                    <span class = "icon-x-alt"></span>
                    Delete
                </button>
                <button type="button" class = "btn btn-quaternary" id = "back-item">
                    <span class = "icon-curved-arrow"></span>
                    Back
                </button>
            </div>          
        </div><!-- box -->  
        </br>
        <div class="widget">
            <div class="widget-content">
                <div class="field-group">
                    <?php //var_dump($item);   ?>
                    <div class="field">
                        <?php echo $form->labelEx($category, 'name'); ?>
                    </div>
                    <div class="field">
                        <?php
                        echo $form->textField($category, 'name', array(
                            'size' => 70,
                            'maxlength' => 255,
                            'class' => 'validate[required]'
                        ));
                        ?>
                    </div>
                    <div class="field">
                        <?php echo $form->error($category, 'name'); ?>
                    </div>                   
                </div>     
                <div class="field-group">
                    <?php //var_dump($item);   ?>
                    <div class="field">
                        <?php echo $form->labelEx($category, 'parent_id'); ?>
                    </div>
                    <div class="field">
                        <?php
                        echo $form->dropDownList($category, 'parent_id', $category->getOptions());
                        ?>
                    </div>
                    <div class="field">
                        <?php echo $form->error($category, 'parent_id'); ?>
                    </div>                   
                </div>     
                <?php $this->endWidget(); ?>
            </div> <!--.grid-->
        </div><!-- widget -->
    </div><!-- widget content -->
</div> <!--.container-->

<div id = "search-result" title = "Search Result">
    <span><br></span>
    Search : 
    <input type="text" id="key-name"/>
    <table class = "table table-striped">
        <thead>
            <tr>
                <th style = "width: 50%">Category Name</th>                    
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

</div>






