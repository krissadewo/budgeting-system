<div id="contentHeader">
    <h1>Item</h1>
</div> <!-- #contentHeader -->
<script type="text/javascript">     
   
    $(document).ready(function(){      
        
        function fillTableItems(data){
            $('#loader').show();
            $('.table.table-striped').children('tbody').empty();
            
            $.each(data['categories'], function(i ,category){         
                var link = '<a href=javascript:; class=choose value='+category.id+'>'+category.name+'</a>'
                $('.table.table-striped').children('tbody').
                    append('<tr><td>'+link+'</td></tr>');                
            })  
           
            $('#loader').hide();
        }        
       
    
        function searchCategory(){
            $.ajax({
                url         : '<?php echo $this->createUrl("category/list/"); ?>',
                data        : {name : $("#key-name").val(), paging : 0},
                dataType    : 'json',
                type        : 'get'               
            }).done(function(data){               
                if(data['categories'].length == 0){
                    $("#dialog-data-not-found").dialog("open");                   
                }else{                   
                    fillTableItems(data);          
                }             
            });         
        }
        
        $(function() {
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
		
            $( "#search-result" ).dialog({
                autoOpen: false,
                height: 480,
                resizable: false,
                width: 650,
                modal: true       
            });
        });           
        
        $(function() {
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
	
            $( "#dialog-delete-confirm" ).dialog({
                autoOpen : false,
                resizable: false,
                height:190,
                width: 350,
                modal: true,
                buttons: {
                    "Delete ": function() {
                        $.ajax({
                            url         : '<?php echo $this->createUrl("item/delete"); ?>',
                            data        : {id : $("#item-id").val()},
                            dataType    : 'json',
                            type        : 'get'                            
                        }).done(function(msg){
                            if(msg['success']){
                                alert(msg['success']);  
                                $(location).attr('href', '<?php echo $this->createUrl('item/index'); ?>');      
                            }else{
                                alert("Failed");
                            }
                        });                           
                       
                        $( this ).dialog( "close" );
                    },
                    Cancel: function() {
                        $( this ).dialog( "close" );
                    }                    
                }
            });
        });
       
        $( ".table.table-striped" ).on('click','.choose',function() {
            //Save val from td content and show it to search field 
            var result =$(this).html().split("::");           
            $("#category-name").val(result[result.length-1].replace(/^ +/,""));    
            //set catgory id to hidden value
            $("#category-id").val($(this).attr('value'));            
            $("#search-result" ).dialog( "close" );
            
        });
        
        $("#back-item").button().click(function(){          
            $(location).attr('href', '<?php echo $this->createUrl('item/index'); ?>'); 
        });
        
        $("#delete-item").button().click(function(){
            $("#dialog-delete-confirm").dialog("open");
        });
        
        $("#save-item").button().click(function(){
            $(location).attr('href', '<?php echo $this->createUrl('item/index'); ?>'); 
        });
        
        $( "#search-category" ).button().click(function() {           
            searchCategory();
            $( "#search-result" ).dialog( "open" );
        });
        
        $("#key-name").change(function(){
            searchCategory();
        });
            
    });         
       
</script>
<div class="container">    
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'item-form',
        'enableAjaxValidation' => FALSE,
            ));
    ?>
    <?php echo $form->hiddenField($item, 'id', array('id' => 'item-id')); ?>
    <div class = "grid-24">
        <div class = "box plain">
            <div class = "dataTables_filter">
                <button type="submit" class = "btn btn-quaternary" id = "save-item">
                    <span class = "icon-document-alt-stroke"></span>
                    Save
                </button>
                <button type="button" class = "btn btn-quaternary" id = "delete-item">
                    <span class = "icon-x-alt"></span>
                    Delete
                </button>
                <button type="button" class = "btn btn-quaternary" id = "back-item">
                    <span class = "icon-curved-arrow"></span>
                    Back
                </button>
            </div>          
        </div><!-- box -->  
        </br>
        <div class="widget">
            <div class="widget-content">
                <div class="field-group">
                    <?php //var_dump($item);   ?>
                    <div class="field">
                        <?php echo $form->labelEx($item, 'name'); ?>
                    </div>
                    <div class="field">
                        <?php
                        echo $form->textField($item, 'name', array(
                            'size' => 70,
                            'maxlength' => 255,
                            'class' => 'validate[required]'
                        ));
                        ?>
                    </div>
                    <div class="field">
                        <?php echo $form->error($item, 'name'); ?>
                    </div>
                </div>            
                <div class="field-group"> 
                    <?php echo $form->hiddenField($item, 'category_id', array('id' => 'category-id')); ?>
                    <div class="field">
                        <?php echo $form->labelEx($item, 'category_id'); ?>
                    </div>
                    <div class="field">
                        <?php
                        echo $form->textField($item->category, 'name', array(
                            'id' => 'category-name',
                            'size' => 70, 'maxlength' => 255,
                            'class' => 'validate[required]',
                            'disabled' => 'true',
                        ));
                        ?>
                        <button type="button" class="btn btn-small btn-quaternary" id="search-category">
                            <span class="icon-magnifying-glass">Search</span>
                        </button>
                    </div>
                    <div class="field">
                        <?php echo $form->error($item, 'category_id'); ?>
                    </div>
                </div> <!-- .field-group -->
                <?php $this->endWidget(); ?>
            </div> <!--.grid-->
        </div><!-- widget -->
    </div><!-- widget content -->
</div> <!--.container-->

<div id = "search-result" title = "Search Result">
    <span><br></span>
    Search : 
    <input type="text" id="key-name"/>
    <table class = "table table-striped">
        <thead>
            <tr>
                <th style = "width: 50%">Category Name</th>                    
            </tr>
        </thead>
        <tbody>

        </tbody>
    </table>

</div>





