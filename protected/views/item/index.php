
<script type="text/javascript">     
   
    $(document).ready(function(){               
       
        init();
        
        function init(){            
            $.ajax({
                url         : '<?php echo $this->createUrl("item/list/page");echo '/' . MockApp::app()->session[ItemController::SESSION_PAGING]?>',
                dataType    : 'json' ,    
                data        : {paging : 1}
            }).done(function(data){  
                currentPage = data['pages'].currentPage;
                pageCount = data['pages'].pageCount;                
                fillTableItems(data, pageCount, currentPage);               
            });
        }        
               
        function fillTableItems(data,pageCount,currentPage){
            $('#loader').show();
            $('.table.table-striped').children('tbody').empty();
            
            $.each(data['items'], function(i ,item){         
                if(pageCount != null){
                    paginate(pageCount, currentPage);
                }
                var linkDetail = '<a href=javascript:; value='+item.id+' class=edit><span>&nbsp;&nbsp;&nbsp;&nbsp;</span></a>';                   
                $('.table.table-striped').children('tbody').
                    append('<tr><td>'+item.name+'</td><td>'+item.category_name+'</td><td>'+linkDetail+'</td></tr>');                
            })  
           
            $('#loader').hide();
        }
        
        function paginate(pageCount, currentPage) {
            
            $("#paginate").paginate({
                count                       : pageCount,
                start                       : currentPage +1 ,           
                display                     : 10,
                border                      : false,
                text_color                  : '#79B5E3',
                background_color            : 'none',	
                text_hover_color            : '#2573AF',
                background_hover_color      : 'none', 
                images                      : false,
                mouse                       : 'press',
                onChange                    : function(page){                     
                    $.ajax({
                        url         : '<?php echo $this->createUrl("item/list/page"); ?>/'+page,
                        data        : {name : $('#search-name').val(), paging : 1},
                        dataType    : 'json',
                        type        : 'get'
                    }).done(function(data){                       
                        fillTableItems(data, null, null);   
                        return false;
                    });
                }
            });
        }
        
        $(function() {
            $( "#dialog:ui-dialog" ).dialog( "destroy" );
		
            $( "#search-form" ).dialog({
                autoOpen    : false,
                height      : 180,
                resizable   : false,
                width       : 350,
                modal       : true,
                buttons     : {
                    "Search": function() {                             
                        $.ajax({
                            url         : '<?php echo $this->createUrl("item/list/"); ?>',
                            data        : {name : $("#search-name").val(), paging : 0},
                            dataType    : 'json',
                            type        : 'get'               
                        }).done(function(data){     
                            if(data['items'].length ==0 ){
                                $( "#dialog-data-not-found" ).dialog("open");                                  
                            }else{
                                fillTableItems(data, pageCount, currentPage); 
                                $('#paginate').hide();    
                                $('#search-result').html(data['items'].length+" records found in database");
                            }                   
                        });
                        
                        $( this ).dialog( "close" );                        
                    }        
                }
            });
        });           
      
        $( "#search-item" ).button().click(function() {
            $( "#search-form" ).dialog( "open" );
        });
        
        $( ".table.table-striped" ).on('click','.edit',function() {
            $(location).attr('href', '<?php echo $this->createUrl('item/create'); ?>/id/'+$(this).attr('value'));           
        });       
        
        $("#add-item").button().click(function(){            
            $(location).attr('href', '<?php echo $this->createUrl('item/create'); ?>'); 
        });            
    });    
        
</script>
<div id="contentHeader">
    <h1>Item</h1>
</div> <!-- #contentHeader -->

<div class="container"> 
    <div id = "loader" style = 'display:none'>
        <img src = "<?php echo MockApp::app()->themeManager->baseUrl . '/images/loaders/big-roller.gif'; ?>" />
    </div>
    <div class = "grid-24">
        <div class = "box plain">
            <div class = "dataTables_filter">
                <button class = "btn btn-quaternary" id = "add-item">
                    <span class = "icon-move-alt2"></span>
                    New Data
                </button>
                <button class = "btn btn-quaternary" id = "search-item">
                    <span class = "icon-magnifying-glass"></span>
                    Search
                </button>
            </div>
        </div>
        <div id = "paginate" class = "jPaginate">
            <div class = "jPag-control-back">
                <a class = "jPag-first" style = "color: rgb(121, 181, 227); ">First</a>
                <span class = "jPag-sprevious">«</span>
            </div>
            <div class = "jPag-control-front" style = "left: 399px; ">
                <span class = "jPag-snext">»</span>
                <a class = "jPag-last" style = "color: rgb(121, 181, 227); ">Last</a>
            </div>
        </div>
        <div id="search-result">
           
        </div>
        <table class = "table table-striped">
            <thead>
                <tr>
                    <th style = "width: 50%">Item Name</th>
                    <th style = "width: 43%">Category Name</th>
                    <th style = "width: 7%" ></th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div> <!--.grid-->
</div> <!--.container-->


<div id = "search-form" title = "Search Item">
    <span><br></span>
    <label for = "name">Item Name</label>
    <input type = "text" name = "name"  size="40" id = "search-name" class = "text ui-widget-content ui-corner-all" />
</div>




