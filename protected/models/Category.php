<?php

/**
 * This is the model class for table "category".
 *
 * The followings are the available columns in table 'category':
 * @property integer $id
 * @property string $name
 * @property datetime $last_modified
 * @property integer $sort_order
 * @property integer $parent_id
 *
 * The followings are the available model relations:
 * @property Item[] $items
 */
class Category extends CActiveRecord {

    const PAGE_SIZE = 9;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Category the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'category';
    }

    protected function afterConstruct() {
        if (!isset($this->id)) {
            $this->name = "";
        }
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('parent_id', 'required'),
            array('sort_order, parent_id', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('name', 'required', 'message' => 'Please enter a value for {attribute}.'),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, name, sort_order, parent_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'items' => array(self::HAS_MANY, 'item', 'category_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'sort_order' => 'Sort Order',
            'parent_id' => 'Parent',
            'last_modified' => 'Last Modified'
        );
    }

    /**
     * This recursif function
     * Check whether categery has parent it self.
     * @param type $category
     * @return type String 
     */
    public function hasParent($category) {
        if ($category->parent_id != 0) {
            $parent = Category::model()->find('id=:id', array(':id' => $category->parent_id));
            return $this->hasParent($parent) . ' :: ' . $category->name;
        } else {
            return $category->name;
        }
    }

    public function getAllCategory($name = null, $paging = true) {
        $data = array();
        $criteria = new CDbCriteria();
        $criteria->alias = 'c';
        if (isset($name)) {
            $criteria->condition = "c.name LIKE '%" . $name . "%'";
        }
        $criteria->order = "c.id DESC";

        if ($paging) {
            $count = Category::model()->count($criteria);
            $pages = new CPagination($count);
            $pages->pageSize = Category::PAGE_SIZE;
            $pages->validateCurrentPage = true;
            $pages->applyLimit($criteria);
            $data['pages'] = $pages;
        }

        //$dependency = new CDbCacheDependency('SELECT MAX(last_modified) FROM Category');
        $data['categories'] = Category::model()->getAllCategoriesWithParent($criteria);
        return $data;
    }

    /**
     * Get all categories with parent
     * @param type int $index 
     * @param type int $pageSize
     * @return type array
     */
    public function getAllCategoriesWithParent($criteria = null) {
        $result = array();
        $categories = Category::model()->findAll($criteria);
        foreach ($categories as $category) {
            $category->name = Category::model()->hasParent($category);
            $result [] = $category;
        }
        return $result;
    }

    /**
     * Used to combobox component to select default category and select it as default
     * @return int(Selected Category)
     */
    public function getSelectedCategory() {
        if (isset($this->parent_id))
            return $this->parent_id;
        return 1;
    }

    public function getOptions() {
        return CHtml::listData($this->getAllCategoriesWithParent(), 'id', 'name');
    }

}