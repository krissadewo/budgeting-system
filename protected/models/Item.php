<?php

/**
 * This is the model class for table "Item".
 *
 * The followings are the available columns in table 'Item':
 * @property integer $id
 * @property string $name 
 * @property string $lastModified
 * @property integer $category_id
 *
 * The followings are the available model relations:
 * @property Category $category
 * @property Purchase[] $purchases
 */
class Item extends CActiveRecord {

    const PAGE_SIZE = 9;

    /**
     * Returns the static model of the specified AR class.
     * @param string $className active record class name.
     * @return Item the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function behaviors() {
        return array(
            'CTimestampBehavior' => array(
                'class' => 'zii.behaviors.CTimestampBehavior',
                'createAttribute' => 'created_date',
                'updateAttribute' => 'modified_date',
            )
        );
    }

    protected function afterConstruct() {
        //Set default value to view. Yii alwasy set empty value as 0
        //I don't know how to fix this yet
        if (!isset($this->id)) {
            $this->name = "";
            $this->category_id = "";
            $this->category = new Category;
        }
    }

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'item';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required', 'message' => 'Please enter a value for {attribute}.'),
            array('name', 'length', 'max' => 255),
            array('category_id', 'required', 'message' => 'Please enter a value for {attribute}.'),
            array('category_id', 'numerical', 'integerOnly' => true, 'message' => 'Please enter a value for {attribute}.'),
            array('last_modified', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'category' => array(self::BELONGS_TO, 'category', 'category_id', 'joinType' => 'INNER JOIN'),
            'purchases' => array(self::HAS_MANY, 'purchase', 'item_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'last_modified' => 'Last Modified',
            'category_id' => 'Category',
        );
    }

    public function getAllItem($name, $paging) {
        $data = array();
        $criteria = new CDbCriteria();
        $criteria->alias = 'i';
        if (isset($name)) {
            $criteria->condition = "i.name LIKE '%" . $name . "%'";
        }
        $criteria->order = "i.id DESC";

        if ($paging) {
            $count = Item::model()->count($criteria);
            $pages = new CPagination($count);
            $pages->pageSize = Item::PAGE_SIZE;
            $pages->validateCurrentPage = true;
            $pages->applyLimit($criteria);
            $data['pages'] = $pages;
        }

        $data['items'] = Item::model()->with('category')->findAll($criteria);
        return $data;
    }

}