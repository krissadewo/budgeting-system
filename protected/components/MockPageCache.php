<?php

class MockPageCache extends CFileCache {

    public function getValue($key) {
        if (isset($_GET['flush']))
            $this->deleteValue($key);
        return parent::getValue($key);
    }

}

?>
