<?php

/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
abstract class MockController extends CController {

    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout = '//layouts/sidebar';

    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu = array();

    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs = array();
    protected $session;

    /**
     * Default admin
     */
    public $theme = 'default';

    public function init() {
        $this->initScript();
    }

    private function initScript() {

        MockApp::app()->theme = $this->theme;

        MockApp::app()->themeManager->setBaseUrl(MockApp::app()->theme->baseUrl);
        MockApp::app()->themeManager->setBasePath(MockApp::app()->theme->basePath);

        //MockApp::app()->clientScript->registerCssFile(MockApp::app()->themeManager->baseUrl . '/stylesheets/reset.css', 'screen');
        //MockApp::app()->clientScript->registerCssFile(MockApp::app()->themeManager->baseUrl . '/stylesheets/text.css', 'screen');
        //MockApp::app()->clientScript->registerCssFile(MockApp::app()->themeManager->baseUrl . '/stylesheets/buttons.css', 'screen');
        //MockApp::app()->clientScript->registerCssFile(MockApp::app()->themeManager->baseUrl . '/stylesheets/theme-default.css', 'screen');
        //MockApp::app()->clientScript->registerCssFile(MockApp::app()->themeManager->baseUrl . '/stylesheets/login.css', 'screen');
        //$script = MockApp::app()->getClientScript()->scriptMap;
        //var_dump(Yii::app()->cache->get('script'));
        // if (!($script = Yii::app()->cache->get('script'))) {
        MockApp::app()->getClientScript()->registerCssFile(MockApp::app()->themeManager->baseUrl . '/stylesheets/all.css', 'screen');
        MockApp::app()->getClientScript()->registerScriptFile(MockApp::app()->themeManager->baseUrl . '/javascripts/all.js');

        //      $script = MockApp::app()->getClientScript()->scriptMap;
        //     var_dump("new Script " . MockApp::app()->getClientScript()->scriptMap);
        //     Yii::app()->cache->set('script', $script);
        //} else {
        //    var_dump("old Script " . $script);
        // MockApp::app()->clientScript->registerCssFile(MockApp::app()->themeManager->baseUrl . '/stylesheets/all.css', 'screen');
        //MockApp::app()->clientScript->registerScriptFile(MockApp::app()->themeManager->baseUrl . '/javascripts/all.js');
        //}


        if (MockApp::app()->request->isAjaxRequest) {
            MockApp::app()->getClientScript()->scriptMap = array(
                'text.css' => false,
                'theme-default.css' => false,
                'login.css' => false,
                'reset.css' => false,
                'all.css' => false,
                'buttons.css' => false,
                'all.js' => false,
                'jquery.js' => false);
        }
    }

    /**
     * Save current page in session for paging,       
     * If user leave page and back again then user will show the last page that user has visited
     */
    protected function handleSessionPaging($sessionName, $currentPage) {
        $this->session = new CHttpSession;
        $this->session->open();
        if (isset($this->session[$sessionName])) {
            $this->session->destroySession($sessionName);
        }
        $this->session[$sessionName] = $currentPage + 1;
        $this->session->close();
    }

    public static function cssActiveMenu($menus = array()) {

        foreach ($menus as $menu) {
            if (MockApp::app()->getController()->getId() == $menu) {
                return "nav active";
            }
        }

        return "nav";
    }

}