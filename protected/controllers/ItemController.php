<?php

class ItemController extends MockController {

    const SESSION_PAGING = "currentPage";
    const SESSION_KEY_NAME = "keyName";

    public function init() {
        parent::init();
        MockApp::app()->clientScript->registerCssFile(MockApp::app()->themeManager->baseUrl .
                '/stylesheets/sample_pages/invoice.css', 'screen');
        MockApp::app()->clientScript->registerCssFile(MockApp::app()->request->baseUrl . "/css/jpaginate.css");
        MockApp::app()->clientScript->registerScriptFile(MockApp::app()->request->baseUrl . "/js/jpaginate.js");
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionCreate() {
        $item = new Item;
        if (isset($_GET['id']))
            $item = $this->loadItemById($_GET['id']);

        if (isset($_POST['Item'])) {
            $item->attributes = $_POST['Item'];
            if ($item->save(true)) {
                //refreshing current page to 1
                $this->handleSessionPaging(ItemController::SESSION_PAGING, 0);
                $this->redirect(array('index'));
            }
        }
        $this->render('form', array('item' => $item));
    }

    public function actionDelete($id) {
        if (MockApp::app()->request->isAjaxRequest) {
            $item = $this->loadItemById($id);
            $data['success'] = false;
            try {
                if ($item->delete()) {
                    $data['success'] = true;
                }
            } catch (CDbException $e) {
                $data['success'] = false;
            }

            echo CJSON::encode($data);
        }
    }

    public function actionList() {
        $name = '';
        $paging = $_GET['paging'];
        if (isset($_GET['name'])) {
            $name = $_GET['name'];
        }

        $data = Item::model()->getAllItem($name, $paging);
        //Because Yii can't load data with relation 
        //We need to convert to another array for encode to json data manually
        if (MockApp::app()->request->isAjaxRequest) {
            $jsonData['items'] = array();
            foreach ($data['items'] as $item) {
                $temp = array();
                $temp['id'] = $item->id;
                $temp['name'] = $item->name;
                $temp['category_name'] = $item->category->name;
                array_push($jsonData['items'], $temp);
            }
            if ($paging) {
                $this->handleSessionPaging(ItemController::SESSION_PAGING, $data['pages']->currentPage);
                $page['currentPage'] = $data['pages']->currentPage;
                $page['pageCount'] = $data['pages']->pageCount;
                $jsonData['pages'] = $page;
            }
            echo CJSON::encode($jsonData);
            Yii::app()->end();
        }
    }

    private function loadItemById($id) {
        return Item::model()->findByPk($id);
    }

}