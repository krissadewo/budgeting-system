<?php

class CategoryController extends MockController {

    const PAGING_SESSION = "currentPage";

    /**
     * Can't using COutputCache, because in this case we used complex paging session handler
     * The problem is the paging in session will be cache to, and then if the user try to back to list
     * the user will be have page that has already cache.. so bad... 
     */
    public function filters() {

//        return array(
//            array(
//                'COutputCache',
//                'duration' => 86400,
//                'requestTypes' => array('GET'),
//                'varyByParam' => array('page'),
//                'dependency' => array(
//                    'class' => 'CDbCacheDependency',
//                    'sql' => 'SELECT MAX(last_modified) FROM Category',
//                )
//            ),
//        );
    }

    public function init() {
        parent::init();
        MockApp::app()->clientScript->registerCssFile(MockApp::app()->themeManager->baseUrl .
                '/stylesheets/sample_pages/invoice.css', 'screen');
        MockApp::app()->clientScript->registerCssFile(MockApp::app()->request->baseUrl . "/css/jpaginate.css");
        MockApp::app()->clientScript->registerScriptFile(MockApp::app()->request->baseUrl . "/js/jpaginate.js");
    }

    public function actionIndex() {
        $this->render('index');
    }

    public function actionCreate() {
        $category = new Category;
        if (isset($_GET['id']))
            $category = $this->loadCategoryById($_GET['id']);

        if (isset($_POST['Category'])) {
            $category->attributes = $_POST['Category'];
            if ($category->save(true)) {
                //refreshing current page to 1
                $this->handleSessionPaging(CategoryController::PAGING_SESSION, 0);
                $this->redirect(array('index'));
            }
        }

        $this->render('form', array('category' => $category));
    }

    public function actionDelete($id) {
        if (MockApp::app()->request->isAjaxRequest) {
            $category = $this->loadCategoryById($id);
            $data['success'] = false;
            try {
                if ($category->delete()) {
                    $data['success'] = true;
                }
            } catch (CDbException $e) {
                $data['success'] = false;
            }

            echo CJSON::encode($data);
        }
    }

    public function loadCategoryById($id) {
        $category = Category::model()->findByPk($id);
        if ($category === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $category;
    }

    public function actionList() {
        $name = '';
        $paging = $_GET['paging'];
        if (isset($_GET['name'])) {
            $name = $_GET['name'];
        }

        $data = Category::model()->getAllCategory($name, $paging);
        //Because Yii can't load data with relation 
        //We need to convert to another array for encode to json data manually
        if (MockApp::app()->request->isAjaxRequest) {
            $jsonData['categories'] = array();
            foreach ($data['categories'] as $item) {
                $temp = array();
                $temp['id'] = $item->id;
                $temp['name'] = $item->name;
                $temp['parentId'] = $item->parent_id;
                array_push($jsonData['categories'], $temp);
            }
            if ($paging) {
                $this->handleSessionPaging(CategoryController::PAGING_SESSION, $data['pages']->currentPage);
                $page['currentPage'] = $data['pages']->currentPage;
                $page['pageCount'] = $data['pages']->pageCount;
                $jsonData['pages'] = $page;
            }
            echo CJSON::encode($jsonData);
            Yii::app()->end();
        }
    }

}